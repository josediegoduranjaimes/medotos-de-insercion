/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import java.util.Scanner;
import ufps.util.colecciones_seed.ListaS;

/**
 *
 * @author jose
 */
public class Prueba_3 {
    
 public static void main (String[]Args){
        Scanner sc = new Scanner(System.in);
        boolean salir=false;
        System.out.println("Escoja la opción del experimento a realizar");
        while(!salir){
            System.out.println("1. Insertion Sort Nodo - 20000 \n"
                    + "2. Insertion Sort Info - 20000 \n"
                    + "3. Insertion Sort Nodo - 200000 \n"
                    + "4. Insertion Sort Info - 200000 \n"
                    + "5. Insertion Sort Nodo - 5000000 \n"
                    + "6. Insertion Sort Info - 5000000 \n"
                    + "7. Insertion Sort Nodo - 20000 - Aleatorio \n"
                    + "8. Insertion Sort Info - 20000 - Aleatorio \n"
                    + "9. Insertion Sort Nodo - 200000 - Aleatorio \n"
                    + "10. Insertion Sort Info - 200000 - Aleatorio \n"
                    + "11. Insertion Sort Nodo - 5000000 - Aleatorio \n"
                    + "12. Insertion Sort Info - 5000000 - Aleatorio \n"
                    + "13. Salir");
            switch(sc.nextInt()){
                case 1:
                    insertionNodo(20000);
                    break;
                case 2:
                    insertionInfo(20000);
                    break;
                case 3:
                    insertionNodo(200000);
                    break;
                case 4:
                    insertionInfo(200000);
                    break;
                case 5:
                    insertionNodo(5000000);
                    break;
                case 6:
                    insertionInfo(5000000);
                    break;
                case 7:
                    insertionNodoAleatorio(20000);
                    break;
                case 8:
                    insertionInfoAleatorio(20000);
                    break;
                case 9:
                    insertionNodoAleatorio(200000);
                    break;
                case 10:
                    insertionInfoAleatorio(200000);
                    break;
                case 11:
                    insertionNodoAleatorio(5000000);
                    break;
                case 12:
                    insertionInfoAleatorio(5000000);
                    break;
                case 13:
                    salir=true;
                    break;
                default:
                    System.out.println("Esa opción con ese experimento no existe");
            }
        }
    }
    
    private static void insertionNodo(int numero) {
        ListaS<Integer> l = new ListaS();
        for(int i=1;i<=numero;i++){
            l.insertarAlInicio(i);
        }
        System.out.println("Cabeza: "+l.get(0).toString());
        long startTime = System.nanoTime();
        l.insertionSortNodo();
        long endTime = System.nanoTime()-startTime;
        
        System.out.println("Tiempo --> "+(endTime +" Nanosegundos"));
    }
    
    private static void insertionInfo(int numero) {
        ListaS<Integer> l = new ListaS();
        for(int i=1;i<=numero;i++){
            l.insertarAlInicio(i);
        }
        System.out.println("Cabeza: "+l.get(0).toString());
        long startTime = System.nanoTime();
        l.insertionSortInfo();
        long endTime = System.nanoTime()-startTime;
        
        System.out.println("Tiempo --> "+(endTime +" Nanosegundos"));
    }
    
    private static void insertionNodoAleatorio(int numero) {
        ListaS<Integer> l = new ListaS();
        for(int i=1;i<=numero;i++){
            l.insertarAlInicio((int) (Math.random() * numero + 1));
        }
        System.out.println("Cabeza: "+l.get(0).toString());
        long startTime = System.nanoTime();
        l.insertionSortNodo();
        long endTime = System.nanoTime()-startTime;
        
        System.out.println("Tiempo --> "+(endTime +" Nanosegundos"));
    }
    
    private static void insertionInfoAleatorio(int numero) {
        ListaS<Integer> l = new ListaS();
        for(int i=1;i<=numero;i++){
            l.insertarAlInicio((int) (Math.random() * numero + 1));
        }
        System.out.println("Cabeza: "+l.get(0).toString());
        long startTime = System.nanoTime();
        l.insertionSortInfo();
        long endTime = System.nanoTime()-startTime;
        
        System.out.println("Tiempo --> "+(endTime +" Nanosegundos"));
    }
}
    
    

